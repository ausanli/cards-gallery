const colors = [
  "#eee6ff",
  "#ddccff",
  "#ccb3ff",
  "#bb99ff",
  "#aa80ff",
  "#9966ff",
  "#884dff",
  "#7733ff",
  "#661aff",
  "#5500ff",
  "#4c00e6",
  "#4400cc",
  "#3b00b3",
  "#330099",
  "#2a0080",
  "#ff1aff",
  "#ff99ff",
  "#ffb3ff",
  "#ffccff",
  "#ff3399",
];

const start = document.querySelector(".start");
const screens = document.querySelectorAll(".screen");
const timeList = document.querySelector("#time-list");
const timeRemaining = document.querySelector("#time");
const board = document.querySelector("#board");

let time = 0;
let score = 0;

start.addEventListener("click", (event) => {
  event.preventDefault();
  screens[0].classList.add("up");
});

timeList.addEventListener("click", (event) => {
  if (event.target.classList.contains("time-btn")) {
    time = Number(event.target.getAttribute("data-time"));
    startGame();
  }
});

board.addEventListener("click", (event) => {
  if (event.target.classList.contains("circle")) {
    score++;
    event.target.remove();
    createRandomCircle();
  }
});

function startGame() {
  screens[1].classList.add("up");
  setInterval(decreaseTime, 1000);
  createRandomCircle();
  setTime(time);
}

function decreaseTime() {
  if (time === 0) {
    finishGame();
  } else {
    let current = --time;
    if (current < 10) {
      current = `0${current}`;
    }
    setTime(current);
  }
}

function setTime(value) {
  timeRemaining.innerHTML = `00:${value}`;
}

function finishGame() {
  timeRemaining.parentNode.classList.add("hide");
  board.innerHTML = `<h1>Your Score: <span class ="primary">${score}</h1>`;
}

function createRandomCircle() {
  const circle = document.createElement("div");
  const size = getRandomNumber(10, 60);
  console.log(size);
  const { width, height } = board.getBoundingClientRect();
  console.log(width, height);
  const x = getRandomNumber(0, width - size);
  const y = getRandomNumber(0, height - size);
  circle.classList.add("circle");
  circle.style.width = `${size}px`;
  circle.style.height = `${size}px`;
  circle.style.top = `${y}px`;
  circle.style.left = `${x}px`;
  circle.style.background = `linear-gradient(90deg, ${getRandomColor()} 0%, ${getRandomColor()} 80%, ${getRandomColor()} 100%)`;
  board.append(circle);
}

function getRandomNumber(min, max) {
  return Math.round(Math.random() * (max - min) + min);
}

function getRandomColor() {
  return colors[Math.floor(Math.random() * colors.length)];
}
 